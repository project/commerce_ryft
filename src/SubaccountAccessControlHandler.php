<?php

namespace Drupal\hotelsubaccounts;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the subscription entity.
 *
 * @see \Drupal\newsletter_signup\Entity\Subscription.
 */
class SubaccountAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   *
   * Link the activities to the permissions. checkAccess is called with the
   * $operation as defined in the routing.yml file.
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        $result = AccessResult::allowedIfHasPermission($account, 'get subaccount');
        break;

      case 'edit':
        $result = AccessResult::allowedIfHasPermission($account, 'update subaccount');
        break;

      case 'delete':
        $result = AccessResult::allowedIfHasPermission($account, 'delete subaccount');
        break;

      default:
        /** @var \Drupal\Core\Access\AccessResult $result */
        $result = parent::checkAccess($entity, $operation, $account);
        break;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * Separate from the checkAccess because the entity does not yet exist, it
   * will be created during the 'add' process.
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'create subaccount');
  }
}
