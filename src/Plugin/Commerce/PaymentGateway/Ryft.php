<?php

namespace Drupal\commerce_ryft\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_ryft\RyftService;
use Drupal\commerce_payment\Exception\DeclineException;

/**
 * Provides the Ryft payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "ryft",
 *   label = "Ryft",
 *   display_label = "Ryft",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_ryft\PluginForm\OffsiteRedirect\PaymentOffsiteForm",
 *   },
 *   js_library = "commerce_ryft/ryft",
 *   requires_billing_information = TRUE,
 * )
 */
class Ryft extends OffsitePaymentGatewayBase implements SupportsRefundsInterface {

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Ryft service.
   *
   * @var \Drupal\commerce_ryft\RyftService
   */
  protected $ryftService;

  /**
   * Get fractional amount.
   */
  public static function toFractional($amount) {
    return $amount * 100;
  }

  /**
   * Get the non-fractional amount.
   */
  public static function toFull($pence) {
    return $pence / 100;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition,
    );

    /** @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory */
    $logger_factory = $container->get('logger.factory');
    $instance->logger = $logger_factory->get('commerce_ryft');
    $instance->languageManager = $container->get('language_manager');
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    $instance->moduleHandler = $container->get('module_handler');
    $instance->ryftService = $container->get('commerce_ryft.service');

    return $instance;
  }

  /**
   * Sets up ryft service
   *
   * @return void
   */
  private function initializeRyftService() {
    $this->ryftService->setMode($this->configuration['mode'] === 'live' ? RyftService::LIVE : RyftService::TEST);
    $this->ryftService->setSecretKey($this->configuration['secret_key']);
    $this->ryftService->setWebhookSecretKey($this->configuration['webhook_secret_key']);
  }

  /**
   * Sets up Ryft Session.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param string $redirect_url
   *   The redirect URL.
   *
   * @return mixed
   *   The session.
   */
  public function setUpRyftSession(PaymentInterface $payment, $redirect_url, $account_id = NULL) {
    // Configure Ryft service.
    $this->initializeRyftService();

    $order = $payment->getOrder();

    // Get billing profile and address.
    $billing_profile = $order->getBillingProfile();
    if (!$billing_profile) {
      throw new PaymentGatewayException('Billing profile not found.');
    }
    $address = $billing_profile->get('address')->first();

    // Get order items.
    $order_items = $order->getItems();

    /** @var \Drupal\commerce_store\Entity\StoreInterface $store */
    $store = $order->getStore();

    // Load order item labels to array.
    $order_item_list = [];
    foreach ($order_items as $order_item) {
      $order_item_list[] = [
        'reference' => $order_item->id(),
        'name' => $order_item->label(),
        'quantity' => intval($order_item->getQuantity()),
        'unitPrice' => self::toFractional($order_item->getUnitPrice()->getNumber()),
        'taxAmount' => 0,
        'totalAmount' => self::toFractional($order_item->getTotalPrice()->getNumber()),
      ];
    }

    $params = array_filter([
      "platformFee" => 0,
      "customerDetails" => array_filter([
        "firstName" => $address->getGivenName(),
        "lastName" => $address->getFamilyName(),
      ]),
      "statementDescriptor" => [
        "descriptor" => $store->getName(),
        "city" => $store->get('address')->first()->getLocality(),
      ],
      "orderDetails" => [
        "items" => $order_item_list
      ],
    ]);

    // Add shipping details if available.
    if (!empty($address->getCountryCode())
      && !empty($address->getPostalCode())) {
      $params["shippingDetails"] = [
        "address" => array_filter([
          "firstName" => $address->getGivenName(),
          "lastName" => $address->getFamilyName(),
          "lineOne" => $address->getAddressLine1(),
          "lineTwo" => $address->getAddressLine2(),
          "postalCode" => $address->getPostalCode(),
          "region" => $address->getAdministrativeArea(),
          "city" => $address->getLocality(),
          "country" => $address->getCountryCode(),
        ]),
      ];
    }

    // Enable modules to alter the session data.
    $this->moduleHandler->alter('commerce_ryft_session_params', $params, $payment, $account_id);

    // @todo Add Line Items - lineItems.
    try {
      $session = $this->ryftService->createPaymentSession(
        self::toFractional($order->getTotalPrice()->getNumber()),
        $order->getTotalPrice()->getCurrencyCode(),
        $order->getEmail(),
        $order->id(),
        $redirect_url,
        $account_id,
        $params);
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      throw new PaymentGatewayException($e->getMessage());
    }
    return $session;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'public_key' => '',
      'secret_key' => '',
      'webhook_secret_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API public key'),
      '#default_value' => $this->configuration['public_key'],
      '#required' => TRUE,
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API secret key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];

    $form['webhook_secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhook secret key'),
      '#default_value' => $this->configuration['webhook_secret_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['public_key'] = $values['public_key'];
      $this->configuration['secret_key'] = $values['secret_key'];
      $this->configuration['webhook_secret_key'] = $values['webhook_secret_key'];
    }
  }

  /**
   * Undocumented function
   *
   * @param [type] $order_id
   * @param [type] $session_id
   * @param [type] $remote_state
   * @param [type] $amount
   * @param [type] $currency
   * @param [type] $event_time
   * @return void
   */
  private function upsertCompletedPayment($order_id, $session_id, $remote_state, $amount, $currency, $event_time) {
    // Update payment object.
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->loadByRemoteId($session_id);
    if (!empty($payment) && $payment->getState() != 'completed') {
      $payment->setState('completed');
      $payment->setRemoteState($remote_state);
      $payment->setAuthorizedTime($event_time);
    }
    else {
      $payment = $payment_storage->create([
        'state' => 'completed',
        'remote_state' => $remote_state,
        'authorized_time' => $event_time,
        'remote_id' => $session_id,
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $order_id,
        'amount' => new Price($amount, $currency),
        'test' => $this->getMode() == 'test',
      ]);
    }
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    // Initialize Ryft service
    $this->initializeRyftService();

    // Get session ID from request.
    $session_id = $request->query->get('sessionId');
    if (!$session_id) {
      throw new PaymentGatewayException('Session ID not provided.');
    }

    // Account ID is optional.
    $account_id = $request->query->get('accountId');

    // Get session from Ryft.
    try {
      $session = $this->ryftService->getPaymentSession($session_id, $account_id);
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      throw new PaymentGatewayException($e->getMessage());
    }

    // Check if payment is successful.
    if (!in_array($session['status'], ['Approved', 'Captured'])) {
      throw new DeclineException('Payment declined.');
    }

    // Calculate event data timestamp.
    $event_date = $session['lastUpdatedTimestamp'];

    // Change ISO event date to timestamp.
    $event_time = strtotime($event_date);
    $remote_state = $session['status'];
    $amount = self::toFull($session['amount']);
    $currency = $session['currency'];

    // Upsert payment object.
    $this->upsertCompletedPayment(
      $order->id(),
      $session_id,
      $remote_state,
      $amount,
      $currency,
      $event_time);
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    // Initialize Ryft service
    $this->initializeRyftService();

    // Get request headers and body.
    $headers = $request->headers->all();
    $payload = json_decode($request->getContent(), TRUE);

    // Get signature from header and validate it.
    $signature = $headers['Signature'][0];
    if (!$this->ryftService->validateWebhookSignature($payload, $signature)) {
      $this->logger->warning('Invalid signature.');
      return;
    }

    // Get event type.
    $event_type = $payload['eventType'];
    switch ($event_type) {
      case 'PaymentSession.captured':
        // Get session ID.
        if (empty($payload['data']['id'])) {
          $this->logger->warning('Session ID not provided.');
          return;
        }
        if (empty($payload['createdTimestamp'])) {
          $this->logger->warning('created timestamp not provided.');
          return;
        }
        if (empty($payload['data']['amount'])) {
          $this->logger->warning('amount not provided.');
          return;
        }
        if (empty($payload['data']['currency'])) {
          $this->logger->warning('currency not provided.');
          return;
        }
        if (empty($payload['data']['metadata']['orderId'])) {
          $this->logger->warning('order ID not provided.');
          return;
        }
        $session_id = $payload['data']['id'];
        $event_time = $payload['createdTimestamp'];
        $amount = self::toFull($payload['data']['amount']);
        $currency = $payload['data']['currency'];
        $order_id = $payload['data']['metadata']['orderId'];

        // Upsert payment object.
        $this->upsertCompletedPayment(
          $order_id,
          $session_id,
          'Captured',
          $amount,
          $currency,
          $event_time);
        break;

      case 'PaymentSession.approved':
        // TODO: Handle approved payment.
        break;

      case 'PaymentSession.declined':
        // TODO: Handle declined payment.
        break;

      case 'PaymentSession.cancelled':
        // TODO: Handle cancelled payment.
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, ?Price $amount = NULL) {
    // Initialize Ryft service
    $this->initializeRyftService();

    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);

    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Extra params.
    $params = [];

    // Sort out account refunds.
    $account_id = NULL;
    \Drupal::moduleHandler()->alter('commerce_ryft_subaccount', $account_id, $payment);

    // Enable modules to alter the session data.
    $this->moduleHandler->alter('commerce_ryft_refund_params', $params, $payment, $account_id);

    try {
      $this->ryftService->createRefund($payment->getRemoteId(),
        self::toFractional($amount->getNumber()), $account_id, $params);
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      throw new PaymentGatewayException($e->getMessage());
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

}
