<?php

namespace Drupal\commerce_ryft\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\commerce_payment\Exception\PaymentGatewayException;

/**
 * Class PaymentOffsiteForm.
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    // Get billing profile and address.
    $billing_profile = $order->getBillingProfile();
    if (!$billing_profile) {
      throw new PaymentGatewayException('Billing profile not found.');
    }
    $address = $billing_profile->get('address')->first();
    $payment_gateway = $payment->getPaymentGateway();
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    // Pass Ryft Drop-in configuration data.
    $form['#attached']['drupalSettings']['ryft']['mode'] = $payment_gateway_plugin->getMode();
    $form['#attached']['drupalSettings']['ryft']['publicKey'] = $configuration['public_key'];
    $form['#attached']['drupalSettings']['ryft']['returnUrl'] = $form['#return_url'];
    $form['#attached']['drupalSettings']['ryft']['billingAddress'] = array_filter([
      "firstName" => $address->getGivenName(),
      "lastName" => $address->getFamilyName(),
      "lineOne" => $address->getAddressLine1(),
      "lineTwo" => $address->getAddressLine2(),
      "postalCode" => $address->getPostalCode(),
      "region" => $address->getAdministrativeArea(),
      "city" => $address->getLocality(),
      "country" => $address->getCountryCode(),
    ]);

    // Invoke commerce_ryft_subaccount alter hook to allow other modules to set it up if needed.
    $account_id = NULL;
    \Drupal::moduleHandler()->alter('commerce_ryft_subaccount', $account_id, $payment);

    $session = $payment_gateway_plugin->setUpRyftSession(
      $payment,
      Url::fromRoute('<current>', [], ['absolute' => TRUE])->toString(),
      $account_id);

    if (empty($session['clientSecret'])) {
      throw new PaymentGatewayException('We are experiencing problems setting up payment session.');
    }

    $form['#attached']['drupalSettings']['ryft']['clientSecret'] = $session['clientSecret'];
    $form['#attached']['drupalSettings']['ryft']['accountId'] = $account_id;

    $form['#process'][] = [get_class($this), 'processRedirectForm'];

    // Add Ryft containers and buttons.
    $form['ryft_container'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'id' => 'ryft-pay-form',
      ],
    ];

    $form['ryft_error_container'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'id' => 'ryft-pay-error',
      ],
    ];

    $form['ryft_button'] = [
      '#type' => 'button',
      '#disabled' => TRUE,
      '#value' => t('Pay'),
      '#attributes' => [
        'id' => 'pay-btn',
        'disabled' => 'disabled',
      ],
    ];

    // Add in Ryft js library.
    $form['#attached']['library'] = ['commerce_ryft/ryft'];

    return $form;
  }

  /**
   * Prepares the complete form for a redirect.
   *
   * Sets the form #action, adds a class for the JS to target.
   * Workaround for buildConfigurationForm() not receiving $complete_form.
   *
   * @param array $form
   *   The plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed form element.
   */
  public static function processRedirectForm(array $form, FormStateInterface $form_state, array &$complete_form) {
    // We make sure that actual Commerce actions are hidden here.
    // The actual redirects are handled via JS embedded component.
    $complete_form['actions']['#access'] = TRUE;
    foreach (Element::children($complete_form['actions']) as $element_name) {
      $complete_form['actions'][$element_name]['#access'] = FALSE;
    }

    // Add cancel link to actions.
    $complete_form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => t('Go Back'),
      '#url' => Url::fromUri($form['#cancel_url']),
    ];

    return $form;
  }

}
