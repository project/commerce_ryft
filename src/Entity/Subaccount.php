<?php

namespace Drupal\commerce_ryft\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Ryft subaccount entity class.
 *
 * @ContentEntityType(
 *   id = "commerce_ryft_subaccount",
 *   label = @Translation("Ryft subaccount"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\commerce_ryft\SubaccountAccessControlHandler",
 *   },
 *   base_table = "commerce_ryft_subaccount",
 *   admin_permission = "administer subaccounts",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   }
 * )
 */

class Subaccount extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['email'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Email'))
      ->setDescription(t('The email of subaccount.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 256,
      ]);

    $fields['ryft_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Ryft Id'))
      ->setDescription(t('Ryft Id of subaccount.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 256,
      ]);

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('The type of subaccount.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 256,
      ]);

    $fields['flow'] = BaseFieldDefinition::create('string')
      ->setLabel(t('flow'))
      ->setDescription(t('The flow of subaccount.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 256,
      ]);

    $fields['onboarding_link'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Onboarding link'))
      ->setDescription(t('The onboarding link of subaccount.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 256,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the booking item was created.'))
      ->setRequired(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the booking was last edited.'))
      ->setRequired(TRUE);

    return $fields;
  }

}
