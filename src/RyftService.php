<?php

namespace Drupal\commerce_ryft;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Ryft API client class.
 */
class RyftService {

  const TEST = 'test';
  const LIVE = 'live';

  /**
   * Ryft API mode.
   *
   * @var string
   */
  private $mode = self::TEST;

  /**
   * Ryft API secret key.
   *
   * @var string
   */
  private $secretKey;

  /**
   * Ryft webhook secret key.
   *
   * @var string
   */
  private $webhookSecretKey;


  /**
   * HTTP Client.
   *
   * @var GuzzleHttp\ClientInterface
   */
  private ClientInterface $httpClient;

  /**
   * Logger factory.
   *
   * @var Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Ryft class Constructor.
   *
   * @param GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param Psr\Log\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(ClientInterface $client, LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $client;
    $this->logger = $logger_factory->get('commerce_ryft');
  }

  /**
   * Sets API mode.
   *
   * @param string $mode
   *   The API mode.
   */
  public function setMode($mode) {
    $this->mode = $mode;
  }

  /**
   * Set API secret key.
   *
   * @param string $secret_key
   *   The API secret key.
   */
  public function setSecretKey($secret_key) {
    $this->secretKey = $secret_key;
  }

  /**
   * Set webhooj secret key.
   *
   * @param string $secret_key
   *   The webhook secret key.
   */
  public function setWebhookSecretKey($secret_key) {
    $this->webhookSecretKey = $secret_key;
  }

  /**
   * Returns API URL based on the API mode.
   *
   * @return string
   *   The API URL.
   */
  protected function getUrl() {
    if ($this->mode === self::LIVE) {
      return 'https://api.ryftpay.com/v1';
    }
    else {
      return 'https://sandbox-api.ryftpay.com/v1';
    }
  }

  /**
   * Validates the webhook signature.
   *
   * @param string $payload
   *   The payload.
   * @param string $signature
   *   The signature.
   *
   * @return bool
   *   TRUE if the signature is valid, FALSE otherwise.
   */
  public function validateWebhookSignature($payload, $signature) {
    $hash = hash_hmac('sha256', $payload, $this->webhookSecretKey);
    return $hash === $signature;
  }

  /**
   * Creates a subaccount on Ryft.
   */
  public function createSubaccount(
    $email,
    $flow = "Hosted",
    array $extra = []
  ) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    $payload = [
      "onboardingFlow" => $flow,
      "email" => $email,
    ];

    if (!empty($extra)) {
      $payload = array_merge($payload, $extra);
    }

    try {
      $response = $this->httpClient->post($this->getUrl() . "/accounts", [
        'headers' => $headers,
        'json' => $payload,
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    $body = (string) $response->getBody()->getContents();
    $this->logger->debug($body);
    $json = json_decode($body, TRUE);
    return $json;
  }

  /**
   * Updates a subaccount on Ryft.
   */
  public function updateSubaccount(
    $subaccount_id,
    array $payload
  ) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    try {
      $response = $this->httpClient->patch($this->getUrl() . "/accounts/" . $subaccount_id, [
        'headers' => $headers,
        'json' => $payload,
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    $body = (string) $response->getBody()->getContents();
    $this->logger->debug($body);
    $json = json_decode($body, TRUE);
    return $json;
  }

  /**
   * Authorize an exisiting Ryft account to link with your account.
   */
  public function authorizeSubaccount($email, $return_url) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    $payload = [
      "email" => $email,
      "redirectUrl" => $return_url,
    ];

    try {
      $response = $this->httpClient->post($this->getUrl() . "/accounts/authorize", [
        'headers' => $headers,
        'json' => $payload,
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    $body = (string) $response->getBody()->getContents();
    $this->logger->debug($body);
    $json = json_decode($body, TRUE);
    return $json;
  }

  /**
   * Generates an anboarding link for a subaccount.
   */
  public function generateOnboardingLink($account_id, $return_url) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    $payload = [
      "accountId" => $account_id,
      "redirectUrl" => $return_url,
    ];

    try {
      $response = $this->httpClient->post($this->getUrl() . "/account-links", [
        'headers' => $headers,
        'json' => $payload,
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    $body = (string) $response->getBody()->getContents();
    $this->logger->debug($body);
    $json = json_decode($body, TRUE);
    return $json;
  }

  /**
   * Creates a person for subaccount on Ryft.
   */
  public function createPerson(
    $account_id,
    $email,
    array $extra = []
  ) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    $payload = [
      "email" => $email,
    ];

    if (!empty($extra)) {
      $payload = array_merge($payload, $extra);
    }

    try {
      $response = $this->httpClient->post($this->getUrl() . "/accounts/" . $account_id . "/persons", [
        'headers' => $headers,
        'json' => $payload,
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    $body = (string) $response->getBody()->getContents();
    $this->logger->debug($body);
    $json = json_decode($body, TRUE);
    return $json;
  }

  /**
   * Updates a person for subaccount on Ryft.
   */
  public function updatePerson(
    $account_id,
    $person_id,
    array $extra = []
  ) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    try {
      $response = $this->httpClient->patch($this->getUrl() . "/accounts/" . $account_id . "/persons/" . $person_id, [
        'headers' => $headers,
        'json' => $extra,
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    $body = (string) $response->getBody()->getContents();
    $this->logger->debug($body);
    $json = json_decode($body, TRUE);
    return $json;
  }

  /**
   * Updates a person for subaccount on Ryft.
   */
  public function deletePerson(
    $account_id,
    $person_id
  ) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    try {
      $response = $this->httpClient->delete($this->getUrl() . "/accounts/" . $account_id . "/persons/" . $person_id, [
        'headers' => $headers,
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    $body = (string) $response->getBody()->getContents();
    $this->logger->debug($body);
    $json = json_decode($body, TRUE);
    return $json;
  }

  /**
   * Lists persons for subaccount on Ryft.
   */
  public function listPersons(
    $account_id
  ) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    try {
      $response = $this->httpClient->get($this->getUrl() . "/accounts/" . $account_id . "/persons", [
        'headers' => $headers
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    $body = (string) $response->getBody()->getContents();
    $this->logger->debug($body);
    $json = json_decode($body, TRUE);
    return $json;
  }

  /**
   * Creates a person for subaccount on Ryft.
   */
  public function createFile(
    $account_id,
    $path,
    $category,
    array $extra = []
  ) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'multipart/form-data'
    ];

    if (!empty($account_id)) {
      $headers['Account'] = $account_id;
    }

    $payload = [
      [
        "name" => "file",
        "content" => fopen($path, 'r'),
      ],
      [
        "name" => "category",
        "content" => $category,
      ],
    ];

    if (!empty($extra)) {
      $payload = array_merge($payload, $extra);
    }

    try {
      $response = $this->httpClient->post($this->getUrl() . "/files", [
        'headers' => $headers,
        'multipart' => $payload,
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    $body = (string) $response->getBody()->getContents();
    $this->logger->debug($body);
    $json = json_decode($body, TRUE);
    return $json;
  }

  /**
   * Lists files uoloaded to Ryft.
   */
  public function listFiles() {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    try {
      $response = $this->httpClient->get($this->getUrl() . "/files", [
        'headers' => $headers
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    $body = (string) $response->getBody()->getContents();
    $this->logger->debug($body);
    $json = json_decode($body, TRUE);
    return $json;
  }

  /**
   * Creates a payment session on Ryft.
   *
   * @param int $amount
   *   The amount to charge.
   * @param string $currency
   *   The currency to charge.
   * @param string $customer_email
   *   The customer email.
   * @param string $order_id
   *   The order id.
   * @param string $return_url
   *   The return URL.
   * @param array $extra
   *   Extra data to send.
   *
   * @return string
   *   The response body.
   */
  public function createPaymentSession($amount, $currency, $customer_email, $order_id, $return_url, $account_id = NULL, array $extra = []) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    if (!empty($account_id)) {
      $headers['Account'] = $account_id;
    }

    $payload = [
      'amount' => $amount,
      'currency' => $currency,
      'customerEmail' => $customer_email,
      'metadata' => [
        'orderId' => $order_id,
      ],
      'returnUrl' => $return_url,
    ];

    if (!empty($extra)) {
      $payload = array_merge($payload, $extra);
    }

    try {
      $response = $this->httpClient->post($this->getUrl() . "/payment-sessions", [
        'headers' => $headers,
        'json' => $payload,
        'http_errors' => FALSE,
      ]);

      if ($response->getStatusCode() == 200) {
        $body = (string) $response->getBody()->getContents();
        $json = json_decode($body, TRUE);
        return $json;
      }
      else {
        $this->logger->error($response->getReasonPhrase());
        throw new \Exception($response->getReasonPhrase());
      }
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

  }

  /**
   * Retrieves a payment session from Ryft.
   *
   * @param string $session_id
   *   The session id.
   *
   * @return void
   *   The response body.
   */
  public function getPaymentSession($session_id, $account_id = NULL, $extra = []) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    if (!empty($account_id)) {
      $headers['Account'] = $account_id;
    }

    try {
      $response = $this->httpClient->get($this->getUrl() . "/payment-sessions" . '/' . $session_id, [
        'headers' => $headers,
        'http_errors' => FALSE,
      ]);

    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    if ($response->getStatusCode() == 200) {
      $body = (string) $response->getBody()->getContents();
      $json = json_decode($body, TRUE);
      return $json;
    }
    else {
      $this->logger->error($response->getReasonPhrase());
      throw new \Exception($response->getReasonPhrase());
    }
  }

  /**
   * Undocumented function
   *
   * @param [type] $session_id
   * @param [type] $amount
   * @return void
   */
  public function createRefund($session_id, $amount, $account_id = NULL, array $extra = []) {
    $headers = [
      'Authorization' => $this->secretKey,
      'Content-Type' => 'application/json'
    ];

    if (!empty($account_id)) {
      $headers['Account'] = $account_id;
    }

    $payload = [
      'amount' => $amount
    ];

    if (!empty($extra)) {
      $payload = array_merge($payload, $extra);
    }

    try {
      $response = $this->httpClient->post($this->getUrl() . "/payment-sessions/" . $session_id . "/refunds", [
        'headers' => $headers,
        'json' => $payload,
        'http_errors' => FALSE,
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      throw $e;
    }

    if ($response->getStatusCode() == 202) {
      $body = (string) $response->getBody()->getContents();
      $json = json_decode($body, TRUE);
      return $json;
    }
    else {
      $this->logger->error($response->getReasonPhrase());
      throw new \Exception($response->getReasonPhrase());
    }
  }

}
