/**
 * @file
 * Defines behaviors for the Ryft form.
 */

(($, Drupal, drupalSettings) => {
  Drupal.ryft = Drupal.ryft || { };

  Drupal.ryft.handlePaymentResult = function (paymentSession, accountID = null) {
    if (
      paymentSession.status === "Approved" ||
      paymentSession.status === "Captured"
    ) {
      var redirectUrl = drupalSettings.ryft.returnUrl + "?sessionId=" + paymentSession.id;
      if (accountID) {
        redirectUrl += "&accountId=" + accountID;
      }

      // Redirect to the return page.
      window.location.href = redirectUrl;

      return;
    }
    if (paymentSession.lastError) {
      const userFacingError = Ryft.getUserFacingErrorMessage(paymentSession.lastError);

      const messages = new Drupal.Message();
      messages.clear();
      messages.add(userFacingError, { type: 'error' });

      $('html, body').animate(
        {
          scrollTop: $('[role="alert"]').offset().top - 200,
        },
        1000,
      );
    }
  }

  Drupal.behaviors.ryft = {
    async attach(context) {
      once('ryft-container', '#ryft-pay-form').forEach(async () => {
        const payButton = $("#pay-btn");
        const message = Drupal.t("Please wait...");
        const throbber = $(Drupal.theme.ajaxProgressThrobber(message));

        var params = {
          publicKey: drupalSettings.ryft.publicKey,
          clientSecret: drupalSettings.ryft.clientSecret,
          localisation: {
            cardNumberPlaceholder: Drupal.t("Card number"),
            expiryMonthPlaceholder: Drupal.t("MM"),
            expiryYearPlaceholder: Drupal.t("YY"),
            cvvPlaceholder: Drupal.t("CVV")
          }
        }

        // Check if billing address has all required values in drupalSettings
        // Add only if all are present, otherwise Ryft will return 400 error.
        if (drupalSettings.ryft.billingAddress.firstName
          && drupalSettings.ryft.billingAddress.lastName
          && drupalSettings.ryft.billingAddress.lineOne
          && drupalSettings.ryft.billingAddress.postalCode
          && drupalSettings.ryft.billingAddress.city
          && drupalSettings.ryft.billingAddress.country
        ) {
          params["fieldCollection"] = {
            billingAddress: {
              display: "hidden",
              value: {
                firstName: drupalSettings.ryft.billingAddress.firstName,
                lastName: drupalSettings.ryft.billingAddress.lastName,
                lineOne: drupalSettings.ryft.billingAddress.lineOne,
                lineTwo: drupalSettings.ryft.billingAddress.lineTwo,
                postalCode: drupalSettings.ryft.billingAddress.postalCode,
                city: drupalSettings.ryft.billingAddress.city,
                country: drupalSettings.ryft.billingAddress.country,
                region: drupalSettings.ryft.billingAddress.region
              }
            }
          }
        }

        if (drupalSettings.ryft.accountId) {
          params.accountId = drupalSettings.ryft.accountId;
        }

        Ryft.init(params);

        Ryft.addEventHandler("walletPaymentSessionResult", e => {
          Drupal.ryft.handlePaymentResult(e.paymentSession, drupalSettings.ryft.accountId);
        });

        Ryft.addEventHandler("cardValidationChanged", e => {
          payButton.prop("disabled", !e.isValid);
        });

        const form = document.getElementById("pay-btn");
        form.addEventListener("click", async (e) => {
          // Display full screen Ajax throbber.
          payButton.after(throbber);
          payButton.prop("disabled", true);

          e.preventDefault();
          const paymentRequest = {
            billingAddress: drupalSettings.ryft.billingAddress,
            threeDsRequestDetails: { deviceChannel: "Browser" },
          };
          Ryft.attemptPayment(paymentRequest)
            .then((paymentSession) => {
              Drupal.ryft.handlePaymentResult(paymentSession, drupalSettings.ryft.accountId);
            })
            .catch((error) => {
              payButton.prop("disabled", false);
              throbber.detach();

              const messages = new Drupal.Message();
              messages.clear();
              messages.add(Drupal.t('We have encountered a technical error while processing your payment. Please try again later.'), { type: 'error' });

              $('html, body').animate(
                {
                  scrollTop: $('[role="alert"]').offset().top - 200,
                },
                1000,
              );
            });
        });
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
