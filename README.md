# Commerce Ryft

The Drupal Commerce Ryft module integrates Ryft's payment solution into Drupal's e-commerce platform.

## Installation

To install the drupal/commerce_ryft module in your Drupal project using Composer, you can follow these quick steps:

### Ensure Composer is Installed
Before beginning, make sure Composer is installed on your system. Composer is a dependency manager for PHP, which Drupal uses. You can check if Composer is installed by running composer --version in your command line. If it's not installed, download and install it from getcomposer.org.

### Navigate to Your Drupal Project Root
Open your command line tool and navigate to the root directory of your Drupal project. This is the directory where your main composer.json file is located.

### Require the Module
Run the following command to require the commerce_ryft module in your project:

```bash
composer require drupal/commerce_ryft
```

This command tells Composer to find the module, download it, and also manage any dependencies the module might have.

### Enable the Module
After Composer successfully installs the module, you need to enable it in your Drupal site. You can do this through the Drupal admin interface or by using Drush. To enable the module using Drush, run:

```bash
drush en commerce_ryft -y
```
